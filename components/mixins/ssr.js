export default {
  methods: {
    async polymorphicFetch(url, ctx) {
      if (process.client) {
        return await fetch(window.location.origin + url).then((r) => r.json())
      } else {
        return await fetch(ctx.$host() + url).then((r) => r.json())
      }
    },
  },
}
