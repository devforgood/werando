import axios from 'axios'
import Vue from 'vue'
export const state = () => ({
  token: null,
  loggedUser: {},
  roles: [],
})

export const getters = {
  token: (state) => state.token,
  loggedUser(state) {
    return state.loggedUser
  },
  isAppAdmin(state) {
    return (state.roles || []).findIndex((x) => x.name === 'APP_ADMIN') >= 0
  },
  isLogged(state) {
    return !!state.token
  },
}

export const mutations = {
  SET_CREDENTIALS(state, { user, token }) {
    state.loggedUser = user
    Vue.set(state, 'roles', user.roles || [])
    state.token = token
  },
}

export const actions = {
  async loadCache({ commit }) {
    commit('SET_CREDENTIALS', {
      user: (await Vue.$localforage.getItem('user')) || {},
      token: (await Vue.$localforage.getItem('token')) || null,
    })
  },
  async authenticate({ commit }, payload) {
    const res = await axios.post(`/api/users/authenticate`, {
      ...payload,
    })
    await Vue.$localforage.setItem('token', res.data.token)
    await Vue.$localforage.setItem('user', res.data.user)
    commit('SET_CREDENTIALS', {
      user: res.data.user,
      token: res.data.token,
    })
    return res
  },
  async logout({ commit }, options = {}) {
    commit('SET_CREDENTIALS', {
      user: {},
      token: null,
    })
    await Vue.$localforage.setItem('token', null)
    await Vue.$localforage.setItem('user', {})
    if (options.routeToLogin === false) {
      return
    }
    this.$router.push(
      this.localePath({
        name: 'index',
      })
    )
  },
}
