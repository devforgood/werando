FROM node:14.15.4-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN apk add --no-cache git

# If you are building your code for production
RUN npm ci

# Bundle app source
COPY . .

RUN npm run build

EXPOSE 3000
ENTRYPOINT HOST=0.0.0.0 npm run start