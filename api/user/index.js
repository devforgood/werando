module.exports = (app) => {
  const router = require('express').Router()
  const db = require('mongoose')

  router.get('/', async (req, res) => {
    res.json(await db.model('user').find({}).populate('roles').exec())
  })
  /**
   * Read single
   */
  router.get('/:id', async (req, res) => {
    res.json(await db.model('user').findById(req.params.id))
  })
  router.post('/password_reset/exchange', async (req, res) => {
    const unusedPasswordReset = await db.model('password_reset').findOne({
      key: req.body.key,
      used: false,
    })
    if (unusedPasswordReset) {
      unusedPasswordReset.used = true
      await db.model('user').updateOne(
        {
          _id: unusedPasswordReset.user,
        },
        {
          $set: {
            password: require('md5')(req.body.password),
          },
        }
      )
      await unusedPasswordReset.save()
      res.status(200).json({
        message: 'SUCCESS',
      })
    } else {
      res.status(400).json({
        message: 'INVALID_KEY',
      })
    }
  })
  /**
   * Password reset hook
   */
  router.get('/password_reset/hook', (req, res) => {
    res.send(`
  <script>
    let key = '${req.query.key}'
    function submit(){
      const options = {
          method: 'POST',
          body: JSON.stringify({
            key:key,
            password: document.querySelector(".password").value
          }),
          headers: {
              'Content-Type': 'application/json'
          }
      }
      fetch('/api/users/password_reset/exchange', options)
          .then(res => res.json())
          .then(res => console.log(res));
      window.alert("Request sent. You can close that window")
    }
  </script>
  <label>Enter your new password</label>
  <input class="password" type="password"/>
  <button onclick="submit()">Change password</button>
  `)
  })
  /**
   * Password reset
   */
  router.post('/password_reset', async (req, res) => {
    if (!req.body.email) {
      return res.status(400).json({
        message: 'EMAIL_REQUIRED',
      })
    } else {
      res.status(200).json({
        message: 'quoued',
      })
    }

    const userId =
      (
        (await db.model('user').findOne({
          email: req.body.email,
        })) || {}
      )._id || null
    if (userId) {
      console.log('Will create a password reset for', req.body.email)
      let passwordReset = await db.model('password_reset').findOne({
        user: userId,
        used: false,
      })
      if (!passwordReset) {
        const key = require('uniqid')('prkey-')
        passwordReset = await db.model('password_reset').create({
          key,
          user: userId,
        })
      }
      db.srv.mailing.sendPasswordReset(req.body.email, passwordReset.key)
    } else {
      console.trace('User not found', req.body.email)
    }
  })
  /**
   * Login
   */
  router.post('/authenticate', async (req, res) => {
    const query = {
      password: require('md5')(req.body.password),
    }
    if (req.body.username) {
      query.username = req.body.username
    }
    if (req.body.email) {
      query.email = req.body.email
    }

    if (!req.body.username && query.email.split('@').length === 1) {
      query.username = query.email
    }

    if (query.email && query.username) {
      query.$or = [
        {
          username: query.username,
        },
        {
          email: query.email,
        },
      ]
      delete query.username
      delete query.email
    }

    const user = await db.model('user').findOne(query).populate('roles').exec()

    if (!user) {
      return res.status(401).json({
        message: 'NOT_FOUND',
      })
    }

    const jwt = require('jsonwebtoken')
    const payload = {
      exp: Math.floor(Date.now() / 1000) + 60 * 60,
      data: {
        userId: user._id,
      },
    }
    jwt.sign(payload, process.env.JWT_SECRET || 'secret', (err, encoded) => {
      if (err) {
        console.error(err)
        res.status(500).send()
      }
      req.session.token = encoded
      res.json({
        token: encoded,
        user,
      })
    })
  })
  /**
   * Change password
   */
  router.put('/password', async (req, res) => {
    res.json(
      await db.model('user').updateOne(
        {
          _id: req.body._id,
        },
        {
          $set: {
            password: require('md5')(req.body.password),
          },
        }
      )
    )
  })
  router.put('/', async (req, res) => {
    if (getInvalidUserMessage(req.body)) {
      return res.status(400).json({
        message: getInvalidUserMessage(req.body),
      })
    }

    res.json(
      await db.model('user').updateOne(
        {
          _id: req.body._id,
        },
        {
          $set: {
            username: req.body.username,
            email: req.body.email,
            ...(req.body.firstname ? { firstname: req.body.firstname } : {}),
            ...(req.body.lastname ? { lastname: req.body.lastname } : {}),
            ...(req.body.phone ? { phone: req.body.phone } : {}),
          },
        }
      )
    )
  })
  /**
   * Create
   */
  router.post('/', async (req, res) => {
    if (getInvalidUserMessage(req.body, false)) {
      return res.status(400).json({
        message: getInvalidUserMessage(req.body, false),
      })
    }
    try {
      const user = await db.model('user').create({
        username: req.body.username || req.body.email.split('@')[0],
        email: req.body.email,
        password: require('md5')(req.body.password),
        roles: [],
      })
      db.srv.mailing.sendAcountCreated(user)

      res.json(user)
    } catch (err) {
      const clientError = {}
      const isDuplicatedError = (field) =>
        err.message.includes('duplicate key error collection') &&
        Object.keys(err.keyPattern).includes(field)
      if (isDuplicatedError('email')) {
        clientError.message = 'DUPLICATED_EMAIL'
      }
      if (isDuplicatedError('username')) {
        clientError.message = 'DUPLICATED_USERNAME'
      }
      console.error(err)
      res.status(500).json(clientError)
    }
  })

  /**
   * Remove
   */
  router.delete('/:id', async (req, res) => {
    res.json(
      await db.model('user').findOneAndRemove({
        _id: req.params.id,
      })
    )
  })

  /**
   *
   * @param {*} item
   * @param {*} isEdit
   * @returns
   */
  function getInvalidUserMessage(item, isEdit = true) {
    if (isEdit && !item._id) {
      return '_ID_REQUIRED'
    }

    if (!item.email) {
      return 'EMAIL_REQUIRED'
    }
    if (isEdit) {
      return
    }
    // if (!item.username) {
    // return 'USERNAME_REQUIRED'
    // }
    if (!item.password) {
      return 'PASSWORD_REQUIRED'
    }
  }

  return router
}
