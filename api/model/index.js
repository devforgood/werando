module.exports = (app) => {
  const router = require('express').Router()
  const db = require('mongoose')
  /**
   * Read all
   */
  router.get('/', (req, res) => {
    const query = {}
    if (req.query.name) {
      query.name = req.query.name
    }
    res.json([
      ...(query.name && db.model(query.name)
        ? [db.model(query.name).schema]
        : []),
    ])
  })
  return router
}
