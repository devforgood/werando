const path = require('path')
const mongoose = require('mongoose')

/**
 * @desc MongoWrapper
 */
exports.MongoWrapper = class MongoWrapper {
  /**
   * Connects to DB
   */
  static init() {
    require('./schemas').MongoSchemas.configure()
    const self = this
    mongoose.connect(process.env.MONGO_URI, {
      dbName: process.env.MONGO_DB,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    const db = mongoose.connection
    db.on('error', (err) => {
      console.log('MONGO FAIL', err)
      process.exit(0)
    })
    db.once('open', function () {
      console.log('MONGO OK')
      self.configure().then(console.log('MONGO CONFIGURED OK '))
    })
  }

  /**
   * Configure schemas and models
   */
  static async configure() {
    /**
     * Themes
     */
    await mongoose.model('event_theme').updateOne(
      {
        name: 'Trekking',
      },
      {
        $set: {
          name: 'Trekking',
        },
      },
      {
        upsert: true,
      }
    )

    await mongoose.model('right').updateOne(
      {
        code: 'EVENT_MANAGE_USERS',
      },
      {
        $set: {
          code: 'EVENT_MANAGE_USERS',
          description: 'Can add/remove event users',
        },
      },
      {
        upsert: true,
      }
    )

    const langCodes = require(path.join(process.cwd(), 'lang', 'codes.json'))
    const updateOrCreateTranslation = (
      locale,
      path,
      value = '',
      checkValue = false
    ) =>
      mongoose.model('translation').updateOne(
        {
          locale,
          path,
          ...(!checkValue
            ? {}
            : {
                $or: [{ value: { $exists: false } }, { value: '' }],
              }),
        },
        {
          $set: {
            locale,
            path,
            ...(value ? { value } : {}),
          },
        },
        {
          ...(checkValue ? {} : { upsert: true }),
        }
      )
    await Promise.all(
      langCodes.map((path) => {
        let value = ''
        if (path.includes('::')) {
          value = path.split('::')[1]
          path = path.split('::')[0]
        }

        return Promise.all([
          updateOrCreateTranslation('example', path, value),
          updateOrCreateTranslation('en', path),
          updateOrCreateTranslation('en', path, value, true),
          updateOrCreateTranslation('es', path),
          updateOrCreateTranslation('fr', path),
        ])
      })
    )

    await mongoose.model('role').updateOne(
      {
        name: 'EVENT_ADMIN',
      },
      {
        $set: {
          name: 'EVENT_ADMIN',
          description: 'Manages an event',
          rights: [
            await mongoose.model('right').findOne({
              code: 'EVENT_MANAGE_USERS',
            }),
          ],
        },
      },
      {
        upsert: true,
      }
    )

    await mongoose.model('role').updateOne(
      {
        name: 'EVENT_USER',
      },
      {
        $set: {
          name: 'EVENT_USER',
          description: 'Assists to event',
          rights: [],
        },
      },
      {
        upsert: true,
      }
    )

    await mongoose.model('role').updateOne(
      {
        name: 'APP_ADMIN',
      },
      {
        $set: {
          name: 'APP_ADMIN',
        },
      },
      {
        upsert: true,
      }
    )

    await mongoose.model('user').updateOne(
      {
        username: 'admin',
      },
      {
        $set: {
          username: 'admin',
          password: require('md5')(process.env.ADMIN_PWD || 'admin'),
          roles: [
            await mongoose.model('role').findOne({
              name: 'APP_ADMIN',
            }),
          ],
        },
      },
      {
        upsert: true,
      }
    )
  }
}
