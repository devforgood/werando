const db = require('mongoose')

module.exports = (router) => {
  db.modelNames().forEach((name) => {
    const schema = db.model(name).schema

    if (!db.model(name).paginate) {
      return
    }

    const stringColumns = Object.keys(schema.paths)
      .filter((key) => {
        return schema.paths[key].instance === 'String'
      })
      .map((key) => schema.paths[key])

    // CREATE
    const createPath = `/${name}${
      name.charAt(name.length - 1) === 's' ? '' : 's'
    }`
    router.post(createPath, async (req, res) => {
      const stringFields = stringColumns.reduce((a, v) => {
        if (req.body[v.path]) {
          a[v.path] = req.body[v.path]
        }
        return a
      }, {})

      await db.model(name).create({
        ...stringFields,
      })
      res.status(201).json({})
    })

    // READ ALL
    const readAllPath = createPath
    router.get(readAllPath, async (req, res) => {
      const query = {}
      Object.keys(req.query).forEach((key) => {
        if (key.includes('query.')) {
          query[key.split('query.').join('')] = req.query[key]
        }
        if (key.includes('q.')) {
          query[key.split('q.').join('')] = req.query[key]
        }
      })
      const items = await db.model(name).paginate(query, {
        page: req.query.page || 1,
        limit: req.query.limit || 20,
        sort: req.query.sort || undefined,
        lean: true,
      })
      res.status(200).json(items)
    })
    console.log(name, readAllPath)

    // READ SINGLE
    const readSinglePath = readAllPath + `/:id`
    router.get(readSinglePath, async (req, res) => {
      const item = await db.model(name).findById(req.params.id)
      res.json(item)
    })

    // UPDATE
    const updatePath = readAllPath
    router.put(updatePath, async (req, res) => {
      const stringFields = stringColumns.reduce((a, v) => {
        if (req.body[v.path]) {
          a[v.path] = req.body[v.path]
        }
        return a
      }, {})
      res.json(
        await db.model(name).updateOne(
          {
            _id: req.body._id,
          },
          {
            $set: {
              ...stringFields,
            },
          }
        )
      )
    })

    // DELETE
    const deletePath = readAllPath + `/:id`
    router.delete(deletePath, async (req, res) => {
      const item = await db.model(name).findByIdAndRemove(req.params.id)
      res.json(item)
    })
  })
}
