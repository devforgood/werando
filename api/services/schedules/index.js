const schedule = require('node-schedule')

const closeOldEventsSchedule = require('./old-events.schedule')
schedule.scheduleJob(
  '* * * * *',
  createSchedule('closeOldEvents', closeOldEventsSchedule, {
    immediate: true,
  })
)

function createSchedule(scheduleName, scheduleFn, options = {}) {
  function run() {
    scheduleFn()
      .then(() => {
        console.log('SCHEDULE', scheduleName, 'FINISH')
      })
      .catch((err) => {
        console.log('SCHEDULE', scheduleName, 'ERR', err)
      })
  }
  if (options.immediate) {
    run()
  }
  return function () {
    run()
  }
}
