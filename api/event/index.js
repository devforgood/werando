module.exports = (app) => {
  const path = require('path')
  const router = require('express').Router()
  const db = require('mongoose')
  const R = require('ramda')

  function injectEventPopulation(query) {
    return query
      .populate({
        path: 'users',
        populate: {
          path: 'user',
        },
      })
      .populate({
        path: 'images',
      })
      .populate({
        path: 'theme',
      })
  }

  /**
   * Create
   */
  router.post('/', async (req, res) => {
    if (getInvalidEventMessage(req.body, false)) {
      return res.status(400).json({
        message: getInvalidEventMessage(req.body, false),
      })
    }

    console.log(req.body)
    const event = await db.model('event').create({
      ...R.pick(['distanceKm', 'durationHours', 'difficulty'], req.body),
      title: req.body.title,
      slug: req.body.slug,
      computedSlug: req.body.slug.split(' ').join('-').toLowerCase(),
      ...(req.body.theme ? { theme: db.Types.ObjectId(req.body.theme) } : {}),
      description: req.body.description,
      date: req.body.date,
    })

    /**
     * @TODO: Assign user roles
    if (!event.users.find((ue) => ue.user === user._id)) {
      event.users.push({
        roles: [
          await db.model('role').findOne({
            name: 'EVENT_USER',
          }),
          await db.model('role').findOne({
            name: 'EVENT_ADMIN',
          }),
        ],
        user: user._id,
      })
      await event.save()
    }
     */

    res.json(event)
  })

  /**
   * Read single
   */
  router.get('/:id', async (req, res) => {
    res.json(
      await injectEventPopulation(
        db.model('event').findById(req.params.id)
      ).exec()
    )
  })

  /**
   * Read all
   */
  router.get('/', async (req, res) => {
    const query = {}
    if (req.query.slug) {
      query.computedSlug = req.query.slug
    }
    res.json(
      await injectEventPopulation(db.model('event').find(query))
        .sort('-date')
        .exec()
    )
  })

  /**
   * Update
   */
  router.put('/', async (req, res) => {
    if (getInvalidEventMessage(req.body)) {
      return res.status(400).json({
        message: getInvalidEventMessage(req.body),
      })
    }

    if (req.body.images) {
      await Promise.all(
        req.body.images.map((image) => {
          return db.model('file').updateOne(
            {
              _id: image._id,
            },
            {
              $set: {
                metadata: image.metadata,
              },
            }
          )
        })
      )
    }

    res.json(
      await db.model('event').updateOne(
        {
          _id: req.body._id,
        },
        {
          $set: {
            ...R.pick(['distanceKm', 'durationHours', 'difficulty'], req.body),
            title: req.body.title,
            slug: req.body.slug,
            computedSlug: req.body.slug.split(' ').join('-').toLowerCase(),
            ...(req.body.theme
              ? {
                  theme: db.Types.ObjectId(
                    req.body.theme._id || req.body.theme
                  ),
                }
              : {}),
            description: req.body.description,
            date: req.body.date,
          },
        }
      )
    )
  })

  /**
   * Remove event image
   */
  router.delete('/:eventId/images/:imageId', async (req, res) => {
    const { eventId, imageId } = req.params

    if (!eventId || !imageId) {
      res.status(400).json({
        message: !eventId
          ? 'EVENT_ID_REQUIRED'
          : !imageId
          ? 'IMAGE_ID_REQUIRED'
          : '',
      })
    }

    const eventDoc = await db.model('event').findById(eventId)
    const isImageAssociatedToEvent =
      (eventDoc.images || []).findIndex(
        (_id) => _id === db.Types.ObjectId(imageId)
      ) >= 0
    if (isImageAssociatedToEvent >= 0) {
      eventDoc.images.pull(db.Types.ObjectId(imageId))
      await eventDoc.save()

      const imageDoc = await db.model('file').findById(imageId)
      const referenceIndex = imageDoc.references.findIndex(
        (r) => r.type === 'event' && r._id === eventId
      )
      if (referenceIndex >= 0) {
        imageDoc.references.splice(referenceIndex, 1)
        if (imageDoc.references.length === 0) {
          await db.model('file').findByIdAndRemove(imageId)
        }
      }
    }

    res.status(200).json({
      message: 'SUCCESS',
    })
  })

  /**
   * Remove
   */
  router.delete('/:id', async (req, res) => {
    res.json(
      await db.model('event').findOneAndRemove({
        _id: req.params.id,
      })
    )
  })

  /**
   * Join event
   */
  router.post('/join_event', app.authenticate(), async (req, res) => {
    const user = await db
      .model('user')
      .findById(req.body.user._id || req.body.user)
    const event = await db
      .model('event')
      .findById(req.body.event._id || req.body.event)

    if (!event.users.find((ue) => ue.user.toString() === user._id.toString())) {
      event.users.push({
        roles: [
          await db.model('role').findOne({
            name: 'EVENT_USER',
          }),
        ],
        user: user._id,
      })
      await event.save()
    }
    res.json({
      message: 'DONE',
    })
  })

  /**
   * unjoin event
   */
  router.post('/unjoin_event', async (req, res) => {
    const user = await db
      .model('user')
      .findById(req.body.user._id || req.body.user)
    const event = await db
      .model('event')
      .findById(req.body.event._id || req.body.event)
      .populate({
        path: 'users.roles',
      })
      .exec()

    const eventUser = event.users.find(
      (u) => u.user.toString() === user._id.toString()
    )

    if (eventUser) {
      eventUser.roles = eventUser.roles.filter((r) => r.name !== 'EVENT_USER')
      if (eventUser.roles.length === 0) {
        eventUser.remove()
      }
    }
    await event.save()

    res.json({
      message: 'DONE',
    })
  })

  /**
   * Upload event images
   */
  router.post(
    '/images',
    require('express-fileupload')(),
    require('express').urlencoded({ extended: true }),
    async function (req, res) {
      const eventId = req.body.eventId
      const file = req.files && req.files.file

      if (!eventId) {
        res.status(400).json({
          message: 'EVENT_ID_REQUIRED',
        })
      }
      if (!file) {
        res.status(400).json({
          message: 'FILE_REQUIRED',
        })
      }

      if (
        !['image/png', 'image/jpg', 'image/jpeg', 'image/gif'].includes(
          file.mimetype
        )
      ) {
        res.status(400).json({
          message: 'INVALID_FORMAT',
        })
      }

      const m = require('moment-timezone')
      m.tz('Europe/Paris')
      const date = m().format('YYYY-MM-DD')
      const ext = require('mime-types').extension(file.mimetype)
      const name = require('uniqid')('ei', `.` + ext)
      const savePath = path.join(process.cwd(), 'uploads', date, name)
      const relativePath = path.join(date, name)

      const eventDoc = await db.model('event').findById(eventId)

      if (!eventDoc) {
        res.status(400).json({
          message: 'INVALID_EVENT_ID',
        })
      }

      const fileDoc = await db.model('file').create({
        type: 'image',
        name: file.name,
        path: relativePath,
        references: [
          {
            type: 'event',
            _id: eventId,
          },
        ],
        metadata: {
          size: file.size,
          encoding: file.encoding,
          mimetype: file.mimetype,
        },
      })
      eventDoc.images = eventDoc.images || []
      eventDoc.images.push(fileDoc._id)
      await eventDoc.save()

      await require('sander').mkdir(path.join(process.cwd(), 'uploads', date))
      file.mv(savePath, function (err) {
        if (err) {
          console.error(err)
          return res.status(500).send()
        }
        res.status(200).json({
          message: 'SUCCESS',
          body: req.body,
        })
      })
    }
  )

  /**
   *
   * @param {*} event
   * @param {*} isEdit
   * @returns
   */
  function getInvalidEventMessage(event, isEdit = true) {
    if (isEdit && !event._id) {
      return '_ID_REQUIRED'
    }
    if (!event.title) {
      return 'TITLE_REQUIRED'
    }
    if (!event.slug) {
      return 'SLUG_REQUIRED'
    }
  }

  return router
}
