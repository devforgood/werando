const mongoosePaginate = require('mongoose-paginate-v2')

/**
 * Manages schemas
 */
exports.MongoSchemas = class MongoSchemas {
  /**
   * Configure schemas
   */
  static configure() {
    const mongoose = require('mongoose')

    const { Schema } = mongoose

    const right = new Schema({
      description: String,
      parent: {
        type: mongoose.ObjectId,
        ref: 'right',
      },
      code: {
        type: String,
        unique: true,
      },
    })
    right.plugin(mongoosePaginate)
    mongoose.model('right', right)

    const role = new Schema({
      name: {
        type: String,
        unique: true,
      },
      description: String,
      rights: [
        {
          type: mongoose.ObjectId,
          ref: 'right',
        },
      ],
    })
    role.plugin(mongoosePaginate)
    mongoose.model('role', role)

    const eventUser = new Schema({
      roles: [
        {
          type: mongoose.ObjectId,
          ref: 'role',
        },
      ],
      user: {
        type: mongoose.ObjectId,
        ref: 'user',
      },
    })
    mongoose.model('event_user', eventUser)

    const eventTheme = new Schema({
      name: {
        type: String,
        unique: true,
      },
    })

    eventTheme.plugin(mongoosePaginate)
    mongoose.model('event_theme', eventTheme)

    const translationsScheme = new Schema({
      locale: {
        type: String,
        index: true,
        enum: ['en', 'fr', 'es'],
      },
      path: {
        type: String,
        unique: true,
        required: true,
      },
      value: {
        type: String,
        default: '',
        required: true,
      },
    })
    translationsScheme.plugin(mongoosePaginate)
    mongoose.model('translation', translationsScheme)

    const eventSchema = new Schema(
      {
        slug: {
          type: String,
          unique: true,
          required: true,
        },
        computedSlug: {
          type: String,
          unique: true,
          required: true,
        },
        title: String,
        description: String,
        theme: {
          type: mongoose.ObjectId,
          ref: 'event_theme',
        },
        date: Date,
        users: [eventUser],
        result: {
          type: String,
          enum: ['success', 'canceled'],
        },
        status: {
          type: String,
          enum: ['open', 'closed', 'archived'],
          required: true,
          default: 'open',
        },

        durationHours: {
          type: Number,
        },
        distanceKm: {
          type: Number,
        },
        difficulty: {
          type: String,
          enum: ['easy', 'medium', 'hard'],
        },

        private: {
          type: Boolean,
          default: false,
        },
        password: {
          type: String,
          default: '',
        },
        images: [
          {
            type: mongoose.ObjectId,
            ref: 'file',
          },
        ],
        attachments: [
          {
            type: mongoose.ObjectId,
            ref: 'file',
          },
        ],
      },
      {
        timestamps: true,
      }
    )
    mongoose.model('event', eventSchema)

    const userSchema = new Schema(
      {
        username: {
          type: String,
          unique: true,
        },
        email: {
          type: String,
          unique: true,
        },
        firstname: String,
        lastname: String,
        phone: String,
        password: String,
        about: String,
        bornCountry: String,
        bornDate: Date,
        avatar: {
          type: mongoose.ObjectId,
          ref: 'file',
        },
        gallery: [
          {
            type: mongoose.ObjectId,
            ref: 'file',
          },
        ],
        roles: [
          {
            type: mongoose.ObjectId,
            ref: 'role',
          },
        ],
      },
      {
        timestamps: true,
      }
    )
    mongoose.model('user', userSchema)

    const passwordResetSchema = new Schema(
      {
        key: {
          type: String,
          unique: true,
        },
        user: {
          type: mongoose.ObjectId,
          ref: 'user',
          required: true,
          index: true,
        },
        used: {
          type: Boolean,
          default: false,
          index: true,
        },
      },
      {
        timestamps: true,
      }
    )
    mongoose.model('password_reset', passwordResetSchema)

    const fileSchema = new Schema(
      {
        type: {
          type: String,
          enum: ['image'],
          required: true,
        },
        name: String,
        description: String,
        path: {
          type: String,
          unique: true,
        },
        /** .References to another schemas (used by) (useful to clean orphans) */
        references: {
          type: Array,
          default: [], // Example: [{type:'event',id:'xxxxxx'}]
          //  types: event, profile_avatar, profile_gallery
        },
        metadata: {
          type: Object,
          default: {},
        },
      },
      {
        timestamps: true,
      }
    )
    fileSchema.plugin(mongoosePaginate)
    mongoose.model('file', fileSchema)
  }
}
