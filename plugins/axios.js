import axios from 'axios'
import Vue from 'vue'
axios.interceptors.request.use(
  async function (config) {
    // Do something before request is sent
    config.withCredentials = true
    config.headers = config.headers || {}
    const token = (await Vue.$localforage.getItem('token')) || null
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  }
)
