import localforage from 'localforage'
import Vue from 'vue'
localforage.config({
  name: process.env.APP_NAME || 'app',
  version: 1.0,
})

Vue.use({
  install() {
    Vue.$localforage = Vue.prototype.$localforage = {
      ...localforage,
      setItem(key, value) {
        key = key.split('__').join('_')
        return localforage.setItem(key, value)
      },
      getItem(key) {
        key = key.split('__').join('_')
        return localforage.getItem(key)
      },
    }
  },
})
